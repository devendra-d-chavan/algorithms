def get_max_subarray(arr, low, high):
    if low == high:
        return (low, high, arr[low])

    mid = (low + high) / 2
    (left_low, left_high, left_sum) = get_max_subarray(arr, low, mid)
    (right_low, right_high, right_sum) = get_max_subarray(arr, mid + 1, high)
    (cross_low, cross_high, cross_sum) = \
                                get_max_crossing_subarray(arr, low, mid, high)

    if left_sum >= right_sum and left_sum >= cross_sum:
        return (left_low, left_high, left_sum)

    if right_sum >= left_sum and right_sum >= cross_sum:
        return (right_low, right_high, right_sum)
    else:
        return (cross_low, cross_high, cross_sum)   

def get_max_crossing_subarray(A, low, mid, high):        
    left_sum = float("-inf")
    sum_val = 0

    i = mid
    while i >= low:    
        sum_val = sum_val + A[i]
        if sum_val > left_sum:
            left_sum = sum_val
    	    max_left = i
        i -= 1

    right_sum = float("-inf")
    sum_val = 0
    
    j = mid + 1
    while j <= high:
        sum_val = sum_val + A[j]
        if sum_val > right_sum:
            right_sum = sum_val
            max_right = j
        j += 1

    return (max_left, max_right, left_sum + right_sum)

def transform(a):
    i = 1
    t = []    
    while i < len(a): 
        t.append(a[i] - a[i - 1])
        i += 1

    return t

mylist = [2, 7, 1, 8, 2, 8, 4, 5, 9, 0, 4, 5];
transformed = transform(mylist)
(i, j, profit) = get_max_subarray(transformed, 0, len(transformed) - 1)
print "i = %d : %d, j = %d : %d, profit : %d" % (i + 1, mylist[i + 1], \
                                                j + 1, mylist[j + 1], \
                                                0 if profit == -1 else profit)

mylist = [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
transformed = transform(mylist)
(i, j, profit) = get_max_subarray(transformed, 0, len(transformed) - 1)
print "i = %d : %d, j = %d : %d, profit : %d" % (i + 1, mylist[i + 1], \
                                                j + 1, mylist[j + 1], \
                                                0 if profit == -1 else profit)

mylist = [5, 10, 4, 6, 7]
transformed = transform(mylist)
(i, j, profit) = get_max_subarray(transformed, 0, len(transformed) - 1)
print "i = %d : %d, j = %d : %d, profit : %d" % (i + 1, mylist[i + 1], \
                                                j + 1, mylist[j + 1], \
                                                0 if profit == -1 else profit)

mylist = [5, 10, 4, 6, 12]
transformed = transform(mylist)
(i, j, profit) = get_max_subarray(transformed, 0, len(transformed) - 1)
print "i = %d : %d, j = %d : %d, profit : %d" % (i + 1, mylist[i + 1], \
                                                j + 1, mylist[j + 1], \
                                                0 if profit == -1 else profit)
