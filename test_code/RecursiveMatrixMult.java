import java.util.Arrays;

public class RecursiveMatrixMult {
	public static void main(String[] args) {
		int[] p = new int[] { 30, 35, 15, 5, 10, 20, 25, 32, 36, 16, 2, 1, 4,
				9, 23, 31, 6, 7, 8, 12, 11 };
		for (int i = 2; i <= p.length; i++) {
			long startTime = System.currentTimeMillis();
			recursiveDriver(p, i);
			long endTime = System.currentTimeMillis();

			System.out.printf("%d \\\\\n", endTime - startTime);
		}
	}

	private static int[][] m;
	private static int counter;

	private static int recursiveMatrixMult(int[] p, int i, int j) {
		if (i == j) {
			m[i][i] = 0;
		} else {
			m[i][j] = Integer.MAX_VALUE;
			for (int k = i; k < j; k++) {
				int cost = recursiveMatrixMult(p, i, k)
						+ recursiveMatrixMult(p, k + 1, j) + p[i - 1] * p[k]
						* p[j];
				counter++;
				if (cost < m[i][j]) {
					m[i][j] = cost;
				}
			}
		}

		return m[i][j];
	}

	private static void recursiveDriver(int[] dimensions, int size) {
		int[] p = Arrays.copyOfRange(dimensions, 0, size);
		m = new int[p.length][p.length];
		counter = 0;

		int cost = recursiveMatrixMult(p, 1, p.length - 1);
		System.out.printf("%d 			& %d		& %d		& ", 
				p.length, cost, counter);
	}
}
