import java.util.Stack;
import java.util.Arrays;

public class LongestIncreasingSubsequence {
	public static void main(String[] args) {
		// int[] a = { 7, 3, 8, 4, 6 };
		// getLongestSubsequence(a);
		//
		// System.out.println();
		// int[] b = { 0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15 };
		// getLongestSubsequence(b);

		System.out.println();
		int[] c = { 37, 93, 0, 23, 79, 65, 49, 81, 67, 8, 32, 29, 96, 76, 15,
		 9, 51, 14, 29, 69 };
		getLongestSubsequence(c);

		// System.out.println();
		// int[] d = { 3, 45, 23, 9, 3, 99, 108, 76, 12, 77, 16, 18, 4 };
		// getLongestSubsequence(d);
		
		System.out.println();
		System.out.println("Using LCS");
		printLongestIncreasingSubsequence(c);
	}

	public static void getLongestSubsequence(int[] a) {
		int len = a.length;
		int[] dp = new int[len];
		int[] prev = new int[len];
		dp[0] = 1;
		prev[0] = -1;

		for (int i = 1; i < len; i++) {
			dp[i] = 1;
			prev[i] = -1;
			for (int j = i - 1; j >= 0; j--) {
				if ((dp[j] + 1 > dp[i]) && a[j] <= a[i]) {
					dp[i] = dp[j] + 1;
					prev[i] = j;
				}
			}
		}

		for (int i = 1; i <= a.length; i++) {
			System.out.printf("l(%d) = %d, a[%d] = %d\n", i, dp[i - 1], i,
					a[i - 1]);
			printLongestSubsequence(a, prev, i - 1);
		}

		int maxLength = 1;
		int bestEnd = 0;
		for (int i = 1; i < dp.length; i++) {
			if (dp[i] > maxLength) {
				maxLength = dp[i];
				bestEnd = i;
			}
		}

		System.out.printf("bestEnd at %d of length %d\n", bestEnd, maxLength);
		printLongestSubsequence(a, prev, bestEnd);
	}

	private static void printLongestSubsequence(int a[], int prev[], int end) {
		Stack<Integer> stack = new Stack<Integer>();
		System.out.print("Longest non decreasing sub sequence : ");

		int k = end;
		stack.push(a[k]);
		while (prev[k] >= 0) {
			stack.push(a[prev[k]]);
			k = prev[k];
		}

		while (!stack.isEmpty()) {
			System.out.print(stack.pop() + " ");
		}

		System.out.println();
	}

	private static void printLongestIncreasingSubsequence(int[] x) {
		int len = x.length;
		int[] y = Arrays.copyOf(x, x.length);
		Arrays.sort(y);

		int[][] dp = new int[len + 1][len + 1];		

		for (int i = len - 1; i >= 0; i--) {
			for (int j = len - 1; j >= 0; j--) {
				if (x[i] == y[j])
					dp[i][j] = dp[i + 1][j + 1] + 1;
				else
					dp[i][j] = Math.max(dp[i + 1][j], dp[i][j + 1]);
			}
		}

		int i = 0, j = 0;
		while (i < len && j < len) {
			if (x[i] == y[j]) {				
				System.out.print(x[i] + " ");
				i++;
				j++;
			} else if (dp[i + 1][j] >= dp[i][j + 1]) {
				i++;
			} else {
				j++;
			}
		}

		System.out.println();
	}
}