#include<iostream>

using namespace std;

int pow(int a, int n);

int main()
{
    int a, n;
    cout<<"Enter a : "<<endl;
    cin>>a;

    cout<<"Enter n : "<<endl;
    cin>>n;

    cout<<"pow("<<a<<", "<<n<<") = "<<pow(a,n)<<endl;
    return 0;
}

int pow(int a, int n)
{
    if(n == 1)
        return a;
    
    int p = 0;
    p = pow(a, n/2);
    p *= p;
    if(n%2 == 1)
    {
        p *= a;
    }

    return p;
}
