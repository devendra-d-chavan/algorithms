import java.util.Arrays;

public class MemoizedMatrixMult {
	public static void main(String[] args) {
		int[] p = new int[] { 30, 35, 15, 5, 10, 20, 25, 32, 36, 16, 2, 1, 4,
				9, 23, 31, 6, 7, 8, 12, 11 };

		for (int i = 2; i <= p.length; i++) {
			long startTime = System.currentTimeMillis();
			memoizedDriver(p, i);
			long endTime = System.currentTimeMillis();

			System.out.printf("%d \\\\\n", endTime - startTime);
		}

		// int[] q = new int[] { 5, 2, 4, 3, 7, 9, 7, 8, 6, 1, 3, 7, 6, 5 };
		// memoizedDriver(q, q.length);
		//
		// calculateMatrixMultCost(q);
	}

	private static int counter;

	private static int memoizedMatrixMult(int[] p) {
		int n = p.length - 1;
		int[][] m = new int[p.length][p.length];

		for (int i = 0; i <= n; i++) {
			for (int j = i; j <= n; j++) {
				m[i][j] = Integer.MAX_VALUE;
			}
		}

		return lookUpChain(m, p, 1, n);
	}

	private static int lookUpChain(int[][] m, int[] p, int i, int j) {
		if (m[i][j] < Integer.MAX_VALUE) {
			return m[i][j];
		}

		if (i == j) {
			m[i][j] = 0;
		} else {
			for (int k = i; k < j; k++) {
				int cost = lookUpChain(m, p, i, k)
						+ lookUpChain(m, p, k + 1, j) + p[i - 1] * p[k] * p[j];
				counter++;
				if (cost < m[i][j]) {
					m[i][j] = cost;
				}
			}
		}

		return m[i][j];
	}

	private static void memoizedDriver(int[] dimensions, int size) {
		int[] p = Arrays.copyOfRange(dimensions, 0, size);
		counter = 0;

		int cost = memoizedMatrixMult(p);
		System.out.printf("%d 			& %d		& %d		& ", 
				p.length, cost, counter);
	}

	private static void calculateMatrixMultCost(int[] p) {
		int multCost = 0;
		for (int i = 1; (i + 1) < p.length; i++) {
			multCost = multCost + p[0] * p[i] * p[i + 1];
		}

		System.out.println("Naive matrix multiplication cost : " + multCost);
	}

	private static void printOptimalParenthesis(int[][] s, int i, int j) {
		if (i == j) {
			System.out.printf("A(%d)", i);
		} else {
			System.out.print('(');
			printOptimalParenthesis(s, i, s[i][j]);
			printOptimalParenthesis(s, s[i][j] + 1, j);
			System.out.print(')');
		}
	}
}
