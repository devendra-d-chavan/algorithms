#include<iostream>

using namespace std;

void remove_dup1(int a[]);
void remove_dup2(int a[]);

int main()
{
    int a[10], b[10];
    int k = 0;
    int repeated_ahead = 0;

    cout<<"Enter the 10 numbers"<<endl;
    for(int i = 0; i < 10; i++)
    {
        cin>>a[i];
    }

    remove_dup2(a);
    return 0;
}

void remove_dup1(int a[])
{
    int repeated_ahead;
    int k = 0;
    int b[10];
    for(int i = 0; i < 10; i++)
    {
        repeated_ahead = 0;
        for(int j = i+1; j < 10; j++)
        {
            if(a[i] == a[j])
            {
                repeated_ahead = 1;
                break;
            }
        }

        if(repeated_ahead == 0)
        {
            b[k++] = a[i];
        }
    }

    cout<<"Array without duplicates"<<endl;
    for(int i = 0; i < k; i++)
    {
        cout<<b[i]<<" ";
    }

    cout<<endl;
}

void remove_dup2(int a[])
{
    int j= 0;
    int b[10];

    b[j++] = a[0];
    for(int i = 1; i < 10; i++)
    {
        if(b[j-1] != a[i])   
        {
            b[j++] = a[i];
        }
    }

    cout<<"Array without duplicates"<<endl;
    for(int i = 0; i < j; i++)
    {
        cout<<b[i]<<" ";
    }

    cout<<endl;
}
